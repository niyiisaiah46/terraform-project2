import boto3
import time

def check_instance_status(ec2_client):
    response = ec2_client.describe_instances()

    for reservation in response['Reservations']:
        for instance in reservation['Instances']:
            instance_id = instance['InstanceId']
            state = instance['State']['Name']
            print(f"Instance ID: {instance_id}, State: {state}")

def main():
    region_name = "eu-west-1"
    ec2_client = boto3.client('ec2', region_name=region_name)

    try:
        while True:
            print("Checking instance status...")
            check_instance_status(ec2_client)
            print("Waiting for 5 minutes...")
            time.sleep(300)  # 300 seconds = 5 minutes

    except KeyboardInterrupt:
        print("Scheduler stopped.")

if __name__ == "__main__":
    main()
