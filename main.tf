provider "aws" {
  region = "eu-west-1"
}

variable "vpc_cidr-block" {}
variable "subnet_cidr_block" {}
variable "env_prefix" {}
variable "my_ip" {}
variable "instance_type" {}
variable "ssh-key" {}
variable "avail_zone" {}
variable "public_key_location" {}

data "aws_ami" "amazon-linux-image" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
}



output "aws_ami_id" {
  value = data.aws_ami.amazon-linux-image.id
}

resource "aws_vpc" "myvpc-app" {
  cidr_block = var.vpc_cidr-block
  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "mysubnet-app1" {
  vpc_id     = aws_vpc.myvpc-app.id
  cidr_block = var.subnet_cidr_block
  tags = {
    Name = "${var.env_prefix}-subnet1"
  }
}

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_vpc.myvpc-app.id
}

resource "aws_route_table" "myapp_rtb" {
  vpc_id = aws_vpc.myvpc-app.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }

  tags = {
    Name = "${var.env_prefix}-rtb"
  }
}

resource "aws_route_table" "myapp-rtb2" {
  vpc_id = aws_vpc.myvpc-app.id
  
  tags = {
    Name: "${var.env_prefix}-rtb2"
  }
}

resource "aws_route" "myapp-rtb2-route" {
  route_table_id = aws_route_table.myapp-rtb2.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.myapp-igw.id
}


resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id      = aws_subnet.mysubnet-app1.id
  route_table_id = aws_route_table.myapp_rtb.id
}

resource "aws_security_group" "myapp-sg" {
  name        = "myapp-sg"
  vpc_id      = aws_vpc.myvpc-app.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}-default-sg"
  }
}



resource "aws_instance" "my-app-server1" {
  ami                    = data.aws_ami.amazon-linux-image.id
  instance_type          = var.instance_type
  key_name               = "test"
  associate_public_ip_address = true
  subnet_id              = aws_subnet.mysubnet-app1.id
  vpc_security_group_ids = [aws_security_group.myapp-sg.id]
  availability_zone      = var.avail_zone     # Update with a valid availability zone in us-west-1

  tags = {
    Name = "${var.env_prefix}-server1"
  }

  user_data = <<-EOF
    #!/bin/bash
    apt-get update && apt-get install -y docker-ce
    systemctl start docker
    usermod -aG docker ec2-user
    docker run -p 8080:8080 nginx
  EOF
}

resource "aws_instance" "my-app-server2" {
  ami                    = data.aws_ami.amazon-linux-image.id
  instance_type          = var.instance_type
  key_name               = "test"
  associate_public_ip_address = true
  subnet_id              = aws_subnet.mysubnet-app1.id
  vpc_security_group_ids = [aws_security_group.myapp-sg.id]
  availability_zone      = var.avail_zone  # Update with a valid availability zone in us-west-1

  tags = {
    Name = "${var.env_prefix}-server2"
  }

  user_data = <<-EOF
    #!/bin/bash
    apt-get update && apt-get install -y docker-ce
    systemctl start docker
    usermod -aG docker ec2-user
    docker run -p 8080:8080 nginx
  EOF
}

resource "aws_instance" "my-app-server3" {
  ami                    = data.aws_ami.amazon-linux-image.id
  instance_type          = var.instance_type
  key_name               = "test"
  associate_public_ip_address = true
  subnet_id              = aws_subnet.mysubnet-app1.id
  vpc_security_group_ids = [aws_security_group.myapp-sg.id]
  availability_zone      = var.avail_zone  # Update with a valid availability zone in us-west-1

  tags = {
    Name = "${var.env_prefix}-server3"
  }

  user_data = <<-EOF
    #!/bin/bash
    apt-get update && apt-get install -y docker-ce
    systemctl start docker
    usermod -aG docker ec
  EOF
} 




